---
title: Programmare in Lua
layout: default
root: ../..
idx: 1.2
description: Un'introduzione a Lua, con inclusa una guida alla portata globale/locale.
redirect_from: /it/chapters/lua.html
---

## Introduzione  <!-- omit in toc -->

In questo capitolo parleremo della programmazione in Lua, degli strumenti necessari, e tratteremo alcune tecniche che troverai probabilmente utili.

- [Programmare](#programmare)
    - [Programmare in Lua](#programmare-in-lua)
- [Editor di codice](#editor-di-codice)
- [Portata locale e globale](#portata-locale-e-globale)
    - [Precedenza alla portata locale](#precedenza-alla-portata-locale)
- [Inclusione di altri script Lua](#inclusione-di-altri-script-lua)


## Programmare

Programmare è l'azione di prendere un problema, come ordinare una lista di oggetti, e tramutarlo in dei passaggi che il computer può comprendere.

Insegnarti i processi logici della programmazione non rientra nell'ambito di questo libro; tuttavia, i seguenti siti sono alquanto utili per approfondire l'argomento:

* [Codecademy](http://www.codecademy.com/) è una delle migliori risorse per imparare come scrivere codice; offre un'esperienza guidata interattiva.
* [Scratch](https://scratch.mit.edu) è una buona risorsa quando si comincia dalle basi assolute, imparando le tecniche di problem solving necessarie per la programmazione.\\
  Scratch è *ideato per insegnare ai bambini* e non è un linguaggio serio di programmazione.
* [Programming with Mosh](https://www.youtube.com/user/programmingwithmosh) is
  a good YouTube series to learn programming.

### Programmare in Lua

Neanche insegnarti come programmare in lua rientra nell'ambito di questo libro.
Tuttavia, se mastichi l'inglese puoi rifarti a quest'altro libro, ["Programming in Lua"](https://www.lua.org/pil/contents.html), per un'eccellente infarinatura sull'argomento. Se invece l'inglese non è il tuo forte, troverai comunque svariate guide in italiano in giro per la rete.


## Editor di codice

Un editor di codice con evidenziamento delle parole chiave è sufficiente per scrivere script in Lua.
L'evidenziamento assegna colori diversi a parole e caratteri diversi, a seconda del loro significato, permettendo quindi di individuare più facilmente eventuali errori e inconsistenze.

Per esempio:

```lua
function ctf.post(team,msg)
    if not ctf.team(team) then
        return false
    end
    if not ctf.team(team).log then
        ctf.team(team).log = {}
    end

    table.insert(ctf.team(team).log,1,msg)
    ctf.save()

    return true
end
```

Nel passaggio qui sopra, le parole chiave `if`, `then`, `end` e `return` sono evidenziate.
E Lo stesso vale per le funzioni interne di Lua come `table.insert`.

Tra gli editor più famosi che ben si prestano a lavorare in Lua, troviamo:

* [VSCode](https://code.visualstudio.com/) - software libero (come Code-OSS e VSCodium), rinomato, e che dispone di [estensioni per il modding su Minetest](https://marketplace.visualstudio.com/items?itemName=GreenXenith.minetest-tools).
* [Notepad++](http://notepad-plus-plus.org/) - Solo per Windows

(ne esistono ovviamente anche altri)


## Portata locale e globale

L'essere locale o globale di una variabile determina da dove è possibile accederci.
Una variabile locale è accessibile soltanto da dove viene definita. Ecco alcuni esempi:

```lua
-- Accessibile dall'interno dello script
local one = 1

function myfunc()
    -- Accessibile dall'interno della funzione
    local two = one + one

    if two == one then
        -- Accessible dall'interno del costrutto if
        local three = one + two
    end
end
```

Mentre le variabili globali sono accessibili da qualsiasi script di qualsiasi mod.

```lua
function one()
    foo = "bar"
end

function two()
    print(dump(foo))  -- Output: "bar"
end

one()
two()
```


### Precedenza alla portata locale

Le variabili locali dovrebbero venire usate il più possibile, con le mod che creano al massimo una globale corrispondente al nome della mod.
Crearne di ulteriori è considerato cattiva programmazione, e Minetest ci avviserà di ciò:

    Assignment to undeclared global 'foo' inside function at init.lua:2

Per ovviare, usa `local`:

```lua
function one()
    local foo = "bar"
end

function two()
    print(dump(foo))  -- Output: nil
end

one()
two()
```

Ricorda che `nil` significa **non inizializzato**.
Ovvero la variabile non è stata ancora assegnata a un valore, non esiste o è stata deinizializzata (cioè impostata a `nil`)

La stessa cosa vale per le funzioni: esse sono variabili di tipo speciale, e dovrebbero essere dichiarate locali, in quanto altre mod potrebbero sennò avere funzioni con lo stesso nome.

```lua
local function foo(bar)
    return bar * 2
end
```

Per permettere alle mod di richiamare le tue funzioni, dovresti creare una tabella con lo stesso nome della mod e aggiungercele all'interno.
Questa tabella è spesso chiamata una API.

```lua
mymod = {}

function mymod.foo(bar)
    return "foo" .. bar
end

-- In un'altra mod o script:
mymod.foo("foobar")
```

## Inclusione di altri script Lua

Il metodo consigliato per includere in una mod altri script Lua è usare *dofile*.

```lua
dofile(core.get_modpath("modname") .. "/script.lua")
```

Uno script può ritornare un valore, che è utile per condividere variabili locali private:

```lua
-- script.lua
return "Hello world!"

-- init.lua
local ret = dofile(core.get_modpath("modname") .. "/script.lua")
print(ret) -- Hello world!
```

Nei [capitoli seguenti](../quality/clean_arch.html) si parlerà nel dettaglio di come suddividere il codice di una mod.
