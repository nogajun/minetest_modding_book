---
title: Traduzione
layout: default
root: ../..
idx: 8.05
marked_text_encoding:
  level: info
  title: Marked Text Encoding
  message: |
    Non hai davvero bisogno di capire come funziona il testo formattato, ma potrebbe aiutarti a capire meglio.

    ```
    "\27(T@miamod)Hello everyone!\27E"
    ```

    * `\27` è il carattere di escape -  è usato per dire a Minetest di far attenzione, in quanto sta per seguire qualcosa di speciale. È usato sia per le traduzioni che per la colorazione del testo.
    * `(T@miamod)` dice che il testo a seguire è traducibile usando il dominio testuale di `miamod`.
    * `Hello everyone!` è il testo in inglese da tradurre, passato alla funzione di traduzione.
    * `\27E` è di nuovo il carattere di escape, dove `E` è usato per segnalare che si è arrivati alla fine.
---

## Introduzione <!-- omit in toc -->

Aggiungere il supporto per le traduzioni nelle tue mod e giochi dà la possibilità a più persone di gustarsele.
Stando a Google Play, il 64% dei giocatori di Minetest Android non usano l'inglese come prima lingua.
Per quanto Minetest non tenga traccia di questo parametro nelle altre piattaforme, vien comunque da sé pensare che una buona parte di giocatrici e giocatori non siano madrelingua inglesi.

Minetest ti permette di localizzare i tuoi contenuti in tante lingue diverse, chiedendoti il testo base in inglese, seguito da dei file di traduzione che mappano le parole/frasi equivalenti nelle altre lingue. Questo processo di traduzione è fatto a lato client, cosicché ogni giocatore possa vedere il contenuto nella propria lingua (se disponibile).


- [Come funziona la traduzione lato client?](#come-funziona-la-traduzione-lato-client)
  - [Testo formattato](#testo-formattato)
  - [File di traduzione](#file-di-traduzione)
- [Formattare una stringa](#formattare-una-stringa)
- [Buona prassi per una buona traduzione](#buona-prassi-per-una-buona-traduzione)
- [Traduzioni lato server](#traduzioni-lato-server)
- [Per concludere](#per-concludere)


## Come funziona la traduzione lato client?

### Testo formattato

Il server ha bisogno di dire ai client come tradurre il testo.
Questo accade grazie alla funzione `core.get_translator(dominiotestuale)`, che abbrevieremo con `S()`:

```lua
local S = core.get_translator("miamod")

core.register_craftitem("miamod:oggetto", {
    description = S("My Item"),
})
```

Il primo parametro di `get_translator` è il dominio testuale, che funge da [spazio dei nomi](https://it.wikipedia.org/wiki/Namespace).
Piuttosto che avere tutte le traduzioni di una lingua salvate nello stesso file, queste possono essere suddivise in domini testuali (un file per dominio per lingua).
È buona norma assegnare al dominio testuale lo stesso nome della mod, onde evitare conflitti tra mod diverse.

Il testo formattato può essere usato nella maggior parte dei casi dove è richiesto un testo fatto per gli esseri umani - come i formspec, i campi di definizioni di un oggetto, `infotext` ecc.
Nel caso dei formspec, tieni presente che dovrai usare la funzione di escape `core.formspec_escape` per una corretta visualizzazione.

Quando il client incontra del testo formattato, come quello passato in `description`, ne andrà a cercare il corrispettivo nel file di traduzione della lingua del giocatore. Se la ricerca non avrà avuto esito positivo, ritornerà quello in inglese.

Nel testo formattato sono presenti il testo sorgente in inglese, il dominio testuale, e qualsivoglia altro parametro passato a `S()`.
Essenzialmente, è una codifica testuale della chiamata a `S` che contiene tutte le informazioni necessarie.

{% include notice.html notice=page.marked_text_encoding %}


### File di traduzione

I file di traduzione sono file che possono essere trovati nella cartella `locale` di ogni mod.
Al momento, l'unico formato supportato è `.tr`, ma è probabile che altri formati più tipici saranno aggiunti in futuro.
I file di traduzione devono essere nominati nel seguente modo: `[dominiotestuale].[codicelingua].tr`.

I file `.tr` iniziano con un commento che ne specifica il dominio, seguito poi da righe che mappano il testo originale in inglese nella lingua del file.

Per esempio, `miamod.it.tr`:

```
# textdomain: miamod
Hello everyone!=Ciao a tutti!
I like grapefruit=Mi piace il pompelmo
```

Dovresti creare dei file di traduzione basati sul codice sorgente delle tue mod/giochi, usando uno strumento come [update_translations](https://github.com/minetest-tools/update_translations).
Questo cercherà tutte le occorrenze di `S(` nel tuo codice Lua, creando in automatico un modello che traduttrici e traduttori potranno usare per tradurre nella loro lingua.
Inoltre, si prenderà cura di aggiornare i file di traduzione ogniqualvolta verranno effettuate modifiche al codice.


## Formattare una stringa

Non è raro dover inserire una variabile dentro una stringa da tradurre.
È importante che il testo non sia semplicemente concatenato, in quanto impedirebbe a chi traduce di cambiare l'ordine delle variabili all'interno della frase.
Al contrario, dovresti usare il seguente sistema di formattazione:

```lua
core.register_on_joinplayer(function(player)
    core.chat_send_all(S("Everyone, say hi to @1!", player:get_player_name()))
end)
```

Se vuoi scrivere letteralmente `@` nella tua frase, dovrai usare una sequenza di escape scrivendo `@@`.

Dovresti evitare di concatenare stringhe *all'interno* di una frase; piuttosto, sarebbe meglio concatenare più frasi come da esempio, in quanto permette a chi traduce di lavorare su stringhe più piccole:

```lua
S("Hello @1!", player_name) .. " " .. S("You have @1 new messages.", #msgs)
```


## Buona prassi per una buona traduzione

* Evita di concatenare il testo, optando invece per formattare le stringhe. Questo permette a chi traduce di avere pieno controllo sull'ordine degli elementi;
* Crea i file di traduzione in automatico usando [update_translations](https://github.com/minetest-tools/update_translations);
* È cosa comune che le variabili cambino il testo circostante, per esempio tramite genere e numero. Risulta spesso difficile trovare qualcosa che si sposi bene in tutti i casi, perciò si tende a cambiare la struttura della frase in modo che risulti sempre corretta ("Hai ottenuto 3 mele" -> "Hai ottenuto mela (x3)");
* Le traduzioni potrebbero essere molto più lunghe o molto più corte rispetto all'originale. Assicurati di lasciare sempre un po' di respiro;
* Non tutte le lingue scrivono i numeri nella stessa maniera, come per esempio `1.000` e `1'000`;
* Non dar per scontato che le altre lingue usino le maiscuole nella stessa maniera della tua.


## Traduzioni lato server

Certe volte ti capiterà di voler sapere quale traduzione di una tal stringa stia venendo visualizzata dal giocatore.
Puoi usare `get_player_information` per ottenere la lingua utilizzata e `get_translated_string` per tradurne il testo formattato.

```lua
local list = {
    S("Hello world!"),
    S("Potato")
}

core.register_chatcommand("find", {
    func = function(name, param)
        local info = core.get_player_information(name)
        local lingua = info and info.language or "en"

        for _, riga in ipairs(lista) do
            local trad = core.get_translated_string(language, riga)
            if trad:contains(query) then
                return riga
            end
        end
    end,
})
```

## Per concludere

Se ben gestita, l'API per le traduzioni permette di rendere mod e giochi più accessibili.

Si tenga comunque conto che Minetest è in continua evoluzione e che l'API verrà probabilmente ampliata in futuro.
Per esempio, il supporto per i file di traduzione *gettext* permetterà l'utilizzo di piattaforme e strumenti consolidati come Weblate, mentre nel frattempo si sta lavorando al supporto per il genere e il numero.
